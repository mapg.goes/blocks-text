package com.blockstext.ui.lembrar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import com.blockstext.NotifyService;
import com.blockstext.R;
import com.blockstext.ToastCustom;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import ListView.layout.NomeArq;
import ListView.layout.NomeArqAdapter;

public class LembrarFragment extends Fragment {

    private Button
            hora,
            minuto,
            dia,
            segundo,
            parar,
            excluir;

    private InterstitialAd
            adblembrar;

    private EditText
            unidade;

    private String
            tempo;

    private Integer
            num;

    private ArrayAdapter
            adapter;

    private String[]
            opcoes,
            opcoes2,
            per;

    private String
            linha = "",
            linha2 = "",
            nomearq,
            nomes,
            numero;

    private ToastCustom
            toastCustom;

    private ViewGroup
            viewGroup;

    private View
            v;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.fragment_lembrar, container, false);

        final ListView nome = root.findViewById(R.id.nomearq);
        final ListView nomenot = root.findViewById(R.id.nomenot);

        final AdRequest adRequest = new AdRequest.Builder().build();

        adblembrar = new InterstitialAd(root.getContext());
        adblembrar.setAdUnitId("ca-app-pub-4427917575266520/7083803664");
        adblembrar.loadAd(adRequest);

        toastCustom = new ToastCustom();
        viewGroup = root.findViewById(R.id.container_toast);
        v = getLayoutInflater().inflate(R.layout.custom_toast, viewGroup);

        unidade = root.findViewById(R.id.tempo);

        dia = root.findViewById(R.id.dia);
        hora = root.findViewById(R.id.hora);
        minuto = root.findViewById(R.id.minuto);
        segundo = root.findViewById(R.id.segundo);
        parar = root.findViewById(R.id.parar);
        excluir = root.findViewById(R.id.excluir);

        per = Read("_Arquivos",
                root.getContext()).split("\n");

            for (String s : per) {

                if (s.contains("X---Excluído---X")) ;

                else {

                    if (s.equals("")) ;
                    else {

                        if (Read(s, root.getContext()).contains("Erro:")) ;
                        else {

                            linha = s + "\n" + linha ;
                        }
                    }
                }
            }

        opcoes = linha.split("\n");

        if (linha.equals("")) opcoes[0] = "Nenhum arquivo criado ainda";

        adapter = new NomeArqAdapter(root.getContext(), MontarLista(opcoes));
        nome.setAdapter(adapter);

        nome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                nomearq = opcoes[i];
                numero = String.valueOf(i);

                toastCustom.showToast("Selecionado: " + opcoes[i], R.drawable.ic_selected, 1, root.getContext(), v);

                if (adblembrar.isLoaded()){
                    adblembrar.show();
                }

            }
        });

        per = Read("_nomes",
                root.getContext()).split("\n");

            for (String s : per) {

                if (s.contains("X---Excluído---X")) ;

                else {

                    if (s.equals("")) ;
                    else {

                        if (Read(s, root.getContext()).contains("Erro:")) ;
                        else {

                            linha2 = s + "\n" + linha2 ;
                        }
                    }
                }
            }

        opcoes2 = linha2.split("\n");

        if (linha2.equals("")) opcoes2[0] = "Nenhuma ativa ainda";

        adapter = new NomeArqAdapter(root.getContext(), MontarLista(opcoes2));
        nomenot.setAdapter(adapter);

        nomenot.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                nomes = opcoes2[i];
                numero = String.valueOf(i);

                toastCustom.showToast("Selecionado: " + opcoes2[i], R.drawable.ic_selected, 1, root.getContext(), v);

                if (adblembrar.isLoaded()){
                    adblembrar.show();
                }
            }
        });

        final String[] _num;

        _num = Read("_num", root.getContext())
                .split("\n");

        excluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NotificationManagerCompat notificar = NotificationManagerCompat.from(root.getContext());

                String nm, nm3 = Read(nomes, root.getContext()), nm2 = "";
                String[] linhas = Read("_nomes", root.getContext())
                        .split("\n");

                if(nm3.contains("Erro:"))
                    toastCustom.showToast(nm3, R.drawable.ic_error, 2, root.getContext(), v);

                else {
                    for (String s : linhas) {

                        if (s.equals(nomes)) ;

                        else {
                            nm2 += s + "\n";
                        }
                    }


                    Write("_nomes",
                            nm2.trim(), root.getContext());

                    nm = Read("_num", root.getContext());
                    Write("_num",
                            nm.replace(_num[Integer.parseInt(numero)], "").trim(), root.getContext());

                    notificar.cancel(Integer.parseInt(_num[Integer.parseInt(numero)]));

                    toastCustom.showToast("Excluído das notificações: " + nomes, R.drawable.ic_excluir_not, 2, root.getContext(), v);
                }
            }
        });


        hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tempo = "h";

                chamadaNotificar(nomearq, Integer.parseInt(numero),
                        String.valueOf(unidade.getText()),
                        root.getContext());
            }
        });

        minuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tempo = "m";

                chamadaNotificar(nomearq, Integer.parseInt(numero),
                        String.valueOf(unidade.getText()),
                        root.getContext());
            }
        });

        segundo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tempo = "s";

                chamadaNotificar(nomearq, Integer.parseInt(numero),
                        String.valueOf(unidade.getText()),
                        root.getContext());
            }
        });

        dia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tempo = "d";

                chamadaNotificar(nomearq, Integer.parseInt(numero),
                        String.valueOf(unidade.getText()),
                        root.getContext());
            }
        });

        parar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(root.getContext(), NotifyService.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(root.getContext(),0,intent,0);
                AlarmManager alarme = (AlarmManager) root.getContext().getSystemService(root.getContext().ALARM_SERVICE);

                alarme.cancel(pendingIntent);
                toastCustom.showToast("Lembrar: OFF", R.drawable.ic_notifications_off, 2, root.getContext(), v);
            }
        });

        return root;
    }

    public String Read(String Caminho, Context context) {

        String conteudo = "";

        try {

            FileInputStream arq = context.openFileInput(  Caminho+".txt");
            int tamanho = arq.available();
            byte[] buff = new byte[tamanho];
            arq.read(buff);
            arq.close();
            conteudo = new String(buff);


        } catch (Exception ex) {

            return "Erro: Nenhum arquivo encontrado";

        }
        return conteudo;
    }

    public void Write (String Caminho, String Texto, Context context) {
        try {

            FileOutputStream arq = context.openFileOutput(Caminho + ".txt", Context.MODE_PRIVATE);
            arq.write(Texto.getBytes());
            arq.close();

        } catch (IOException e) {

        }
    }

    private ArrayList<NomeArq> MontarLista(String[] opcoes){
        ArrayList<NomeArq> nome = new ArrayList<NomeArq>();
        for (String s: opcoes) {

            NomeArq addnomes = new NomeArq(s);
            nome.add(addnomes);

        }
        return nome;
    }

    public void chamadaNotificar(String Nome, int numero, String Tempo, Context context) {
        String nm = Read(Nome, context);

        num =  Integer.parseInt(Tempo);
        long time = 0;

        switch (tempo){
            case "s":
                time =  1000 * num;
                break;
            case "m":
                time =  60 * 1000 * num;
                break;
            case "h":
                time = 60 * 60 * 1000 * num;
                break;
            case "d":
                time = 24 * 60 * 60 * 1000 * num;
                break;
        }



        if(nm.contains("Erro:"))
            Toast.makeText(context, "Arquivo não encontrado\n Verifique se o nome esta correto", Toast.LENGTH_LONG).show();

        else {

            acionarAlarme(context, time, Nome, numero);
        }

    }

    public void acionarAlarme(Context context, long time, String Nome, int numero){

        String _nome = Read("_nomes", context);
        String _num = Read("_num", context);
        String[] linhas = Read("_nomes", context)
                .split("\n");

        if (_nome.contains("Erro:")) {

            Write("_nomes", Nome,
                    context);

            Write("_num", String.valueOf(numero),
                    context);

            Intent intent_notify = new Intent(context, NotifyService.class);
            PendingIntent pendingIntent =
                    PendingIntent.getBroadcast(context,0,intent_notify,0);


            AlarmManager alarme = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

            toastCustom.showToast("Lembrar: ON", R.drawable.ic_lampada, 1, context, v);

            alarme.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(),time,pendingIntent);

        }

        else {
            boolean val = false;

            for (String s : linhas ) {

                if (s.equals(Nome)) {
                    Intent intent_notify = new Intent(context, NotifyService.class);
                    PendingIntent pendingIntent =
                            PendingIntent.getBroadcast(context, 0, intent_notify, 0);


                    AlarmManager alarme = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

                    toastCustom.showToast("Notificação já criada, o tempo foi atualizado", R.drawable.ic_atualizar, 1, context, v);

                    alarme.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), time, pendingIntent);

                    val = true;
                }
            }

            if(val);

            else {
                    Write("_nomes",
                            Nome.trim() + "\n" + _nome, context);

                    if (_num.contains("Erro:")){

                        Write("_num", String.valueOf(numero),
                                context);

                        Intent intent_notify = new Intent(context, NotifyService.class);
                        PendingIntent pendingIntent =
                                PendingIntent.getBroadcast(context,0,intent_notify,0);


                        AlarmManager alarme = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

                        toastCustom.showToast("Lembrar: ON", R.drawable.ic_lampada, 1, context, v);

                        alarme.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(),time,pendingIntent);

                    }

                    else{

                        if (_num.contains(String.valueOf(numero))){

                            Intent intent_notify = new Intent(context, NotifyService.class);
                            PendingIntent pendingIntent =
                                    PendingIntent.getBroadcast(context,0,intent_notify,0);


                            AlarmManager alarme = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

                            toastCustom.showToast("Lembrar: ON", R.drawable.ic_lampada, 1, context, v);

                            alarme.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(),time,pendingIntent);
                        }

                        else {
                        Write("_num",
                                String.valueOf(numero).trim() + "\n" + _num, context);

                        Intent intent_notify = new Intent(context, NotifyService.class);
                        PendingIntent pendingIntent =
                                PendingIntent.getBroadcast(context,0,intent_notify,0);


                        AlarmManager alarme = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

                            toastCustom.showToast("Lembrar: ON", R.drawable.ic_lampada, 1, context, v);

                        alarme.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(),time,pendingIntent);

                    }
                }
            }
        }

    }
}