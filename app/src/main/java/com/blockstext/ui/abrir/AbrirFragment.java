package com.blockstext.ui.abrir;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.blockstext.MainActivity;
import com.blockstext.R;
import com.blockstext.ToastCustom;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import ListView.layout.NomeArq;
import ListView.layout.NomeArqAdapter;

public class AbrirFragment extends Fragment {

    private ArrayAdapter
            adapter;

    private InterstitialAd
            abrir;

    private String[]
            opcoes,
            per;

    private String
            linha = "";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater
                .inflate(R.layout.fragment_abrir, container, false);
        final ListView nome = root.findViewById(R.id.nomearq);

        AdRequest adRequest = new AdRequest.Builder().build();

        abrir = new InterstitialAd(root.getContext());
        abrir.setAdUnitId("ca-app-pub-4427917575266520/2119353225");
        abrir.loadAd(adRequest);

        per = Read("_Arquivos",
                root.getContext()).split("\n");

        for (String s : per) {

            if (s.contains("X---Excluído---X")) ;

            else {

                if (s.equals("")) ;
                else {

                    if (Read(s, root.getContext()).contains("Erro:")) ;
                    else {

                        linha += s + "\n";
                    }
                }
            }
        }

        opcoes = linha.split("\n");

        if (linha.equals("")) opcoes[0] = "Não foram criados ainda";

        adapter = new NomeArqAdapter(root.getContext(), MontarLista(opcoes));
        nome.setAdapter(adapter);

        nome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String nomes = Read(opcoes[i], root.getContext());

                ToastCustom toastCustom = new ToastCustom();
                ViewGroup viewGroup = root.findViewById(R.id.container_toast);
                View v = getLayoutInflater().inflate(R.layout.custom_toast, viewGroup);

                if(nomes.contains("Erro:"))
                    toastCustom.showToast(nomes, R.drawable.ic_error, 2, root.getContext(), v);

                else {

                    abrirTexto(root.getContext(), opcoes[i]);
                }

                if (abrir.isLoaded()) {
                    abrir.show();
                }
            }
        });

        return root;
    }

    private ArrayList<NomeArq> MontarLista(String[] opcoes){
        ArrayList<NomeArq> nome = new ArrayList<NomeArq>();
        for (String s: opcoes) {

            NomeArq addnomes = new NomeArq(s);
            nome.add(addnomes);

        }
        return nome;
    }

    public void abrirTexto(Context context, String nome){

        Intent abrir_texto = new Intent(context, MainActivity.class);

        Write("_abrirNome", nome, context);
        Write("_abrirTexto", Read(nome, context), context);
        Write("_setAbrir", "Abrir", context);

        startActivity(abrir_texto);

    }

    public String Read(String Caminho, Context context) {

        String conteudo = "";

        try {

            FileInputStream arq = context.openFileInput(  Caminho+".txt");
            int tamanho = arq.available();
            byte[] buff = new byte[tamanho];
            arq.read(buff);
            arq.close();
            conteudo = new String(buff);


        } catch (Exception ex) {

            return "Erro: Nenhum arquivo encontrado";

        }
        return conteudo;
    }

    public void Write (String Caminho, String Texto, Context context){
        try {

            FileOutputStream arq = context.openFileOutput(Caminho+".txt", Context.MODE_PRIVATE);
            arq.write(Texto.getBytes());
            arq.close();

        }catch (IOException e){

        }
    }
}