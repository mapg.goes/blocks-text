package com.blockstext.ui.criar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.blockstext.MainActivity;
import com.blockstext.R;
import com.blockstext.ToastCustom;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CriarFragment extends Fragment {

    private EditText
            nome;

    private InterstitialAd
            adbcriar;

    private Button
            criar;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater
                .inflate(R.layout.fragment_criar, container, false);

        final AdRequest adRequest = new AdRequest.Builder().build();

        adbcriar = new InterstitialAd(root.getContext());
        adbcriar.setAdUnitId("ca-app-pub-4427917575266520/1345149990");
        adbcriar.loadAd(adRequest);

        nome = root.findViewById(R.id.nomearq);
        criar = root.findViewById(R.id.criar_novo);

        criar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nm = Read("_Arquivos", root.getContext());

                if (nm.contains("Erro:")) {

                    Write("_Arquivos",
                            String.valueOf(nome.getText()).trim(),
                            root.getContext());

                    criarTexto(root.getContext(),
                            String.valueOf(nome.getText()).trim());

                } else {

                    String nm2 = "";
                    String[] linhas = Read("_Arquivos", root.getContext())
                            .split("\n");

                    boolean val = false;

                    for (String s : linhas ){

                        if (s.equals(String.valueOf(nome.getText()).trim())){
                            String rever = Read(s, root.getContext());

                            if (rever.contains("Erro"));

                            else {

                                nm2 += s.trim() + "\n";

                                ToastCustom toastCustom = new ToastCustom();
                                ViewGroup viewGroup = root.findViewById(R.id.container_toast);
                                View v = getLayoutInflater().inflate(R.layout.custom_toast, viewGroup);
                                toastCustom.showToast("O arquivo já existe",R.drawable.ic_error, 2, root.getContext(), v);

                                val = true;
                            }

                        }

                        else{

                            if (s.equals(""));

                            else {
                                nm2 += s.trim() + "\n";

                            }
                        }
                    }

                    if (val);

                    else {
                        Write("_Arquivos",
                                String.valueOf(nome.getText()).trim() + "\n" + nm2, root.getContext());

                        criarTexto(root.getContext(),
                                String.valueOf(nome.getText()).trim());
                    }

                }

                if (adbcriar.isLoaded()){
                    adbcriar.show();
                }

            }
        });

        return root;
    }

    public void criarTexto(Context context, String nome){

        Intent criar_texto = new Intent(context, MainActivity.class);

        Write(nome, "", context);
        Write("_abrirNome", nome, context);
        Write("_abrirTexto", "", context);
        Write("_setAbrir","Abrir", context);

        startActivity(criar_texto);

    }
    public void Write (String Caminho, String Texto, Context context){
        try {

            FileOutputStream arq = context.openFileOutput(Caminho+".txt", Context.MODE_PRIVATE);
            arq.write(Texto.getBytes());
            arq.close();

        }catch (IOException e){

        }
    }
    public String Read(String Caminho, Context context) {

        String conteudo = "";

        try {

            FileInputStream arq = context.openFileInput(  Caminho+".txt");
            int tamanho = arq.available();
            byte[] buff = new byte[tamanho];
            arq.read(buff);
            arq.close();
            conteudo = new String(buff);


        } catch (Exception ex) {

            return "Erro: Nenhum arquivo encontrado";

        }
        return conteudo;
    }
}