package com.blockstext;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationManagerCompat;

import java.io.FileInputStream;


public class NotifyService extends BroadcastReceiver {

    private String[]
            nomes,
            textos;

    private String
            num,
            linhas = "";

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent abrir = new Intent(context, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(context, 0,abrir,0);

        Notification.Builder builder = new Notification.Builder(context, "BlocksText");
        NotificationManagerCompat notificar = NotificationManagerCompat.from(context);
        nomes = Read("_nomes",
                context).split("\n");

        num = Read("_num",
                context);

        int in = 0;

        for (String s : nomes ){


            if (s.equals(""));
            else{

                if (Read(s, context).contains("Erro:"));
                else {

                    while (!num.contains(String.valueOf(in))){

                        in++;
                    }
                    textos = Read(s,context)
                            .split("\n");

                    for (String linha : textos){

                        if (linha.equals(""));
                        else{

                            linhas += " - " + linha + "\n";

                        }

                    }

                    builder.setContentTitle(s)
                            .setTicker(s)
                            .setContentText(linhas)
                            .setSmallIcon(R.drawable.ic_not)
                            .setStyle(new Notification.BigTextStyle().bigText(linhas))
                            .setContentIntent(pi);

                    notificar.notify(in, builder.build());

                    linhas = "";
                    in++;

                }
            }
        }

        Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone toque = RingtoneManager.getRingtone(context, som);
        toque.setVolume(1000);
        toque.play();

        Vibrator rr = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long milliseconds = 2000;
        rr.vibrate(milliseconds);

    }
    public String Read(String Caminho, Context context) {

        String conteudo = "";

        try {
            FileInputStream arq = context.openFileInput(  Caminho+".txt");
            int tamanho = arq.available();
            byte[] buff = new byte[tamanho];
            arq.read(buff);
            arq.close();
            conteudo = new String(buff);

        } catch (Exception ex) {

            return "Erro: Arquivo não encontrado";

        }
        return conteudo;
    }

}

