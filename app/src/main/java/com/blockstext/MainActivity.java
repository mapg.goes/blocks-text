package com.blockstext;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.blockstext.ui.salvar.SalvarFragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity
        extends AppCompatActivity
        implements SalvarFragment.SalvarValues {

    private AppBarConfiguration
            mAppBarConfiguration;

    private BottomNavigationView
            navView;

    private AdView
            banner;

    private TextView
            nome;

    private EditText
            texto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_retratil);
        createNotificationChannel();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer;
        NavigationView navigationView;

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        banner = findViewById(R.id.adView);
        texto = findViewById(R.id.texto);
        nome = findViewById(R.id.text_view);

        String set = Read("_set");

        if(set.contains("Abrir")){

            nome.setText(Read("_nome"));
            texto.setText(Read("_texto"));
            WriteSet("_set", "off");

        }

        String setAbrir = Read("_setAbrir");

        if (setAbrir.contains("Abrir")){
            nome.setText(Read("_abrirNome"));
            texto.setText(Read("_abrirTexto"));
            WriteSet("_setAbrir", "off");
        }

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();
        banner.loadAd(adRequest);

        navView = findViewById(R.id.nav_view_bottom);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    public void Write (String Caminho, String Texto){
        try {

            FileOutputStream arq = openFileOutput(Caminho+".txt", Context.MODE_PRIVATE);
            arq.write(Texto.getBytes());
            arq.close();

            ToastCustom toastCustom = new ToastCustom();
            ViewGroup view = findViewById(R.id.container_toast);
            View v = getLayoutInflater().inflate(R.layout.custom_toast, view);
            toastCustom.showToast("Arquivo Salvo com sucesso", R.drawable.ic_salvar, 1, this, v);

        }catch (IOException e){

            Toast.makeText(MainActivity.this, "Não foi possível salvar arquivo", Toast.LENGTH_LONG).show();
        }
    }

    public void WriteSet(String Caminho, String Texto){
        try {

            FileOutputStream arq = openFileOutput(Caminho+".txt", Context.MODE_PRIVATE);
            arq.write(Texto.getBytes());
            arq.close();

        }catch (IOException e){

        }
    }

    public String Read(String Caminho) {

        String conteudo = "";

        try {

            FileInputStream arq = openFileInput(  Caminho+".txt");
            int tamanho = arq.available();
            byte[] buff = new byte[tamanho];
            arq.read(buff);
            arq.close();
            conteudo = new String(buff);


        } catch (Exception ex) {

            return "Erro: Nenhum arquivo encontrado";

        }
        return conteudo;
    }


    public void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence nome = "Lembrar";
            String descricao = "notificaçoes dos arquivos a serem lembrados";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("BlocksText", nome, importance);
            channel.setDescription(descricao);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_retratil, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void salvarText(String Nome) {
        Write(Nome, String.valueOf(texto.getText()));
    }
}