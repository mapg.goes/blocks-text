package com.blockstext;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

public class ToastCustom {

    public void showToast(String texto, int img, int id, Context context, View v){
        TextView text_toast = v.findViewById(R.id.text_toast);

        switch (id){
            case 1:
                v.setBackground(ContextCompat.getDrawable(context, R.drawable.intent_positivy));
                text_toast.setTextColor(Color.parseColor("#FF424242"));
                break;

            case 2:
                v.setBackground(ContextCompat.getDrawable(context, R.drawable.intent_negativy));
                text_toast.setTextColor(Color.WHITE);
                break;
        }


        text_toast.setText(texto);

        ImageView icon = v.findViewById(R.id.img_toast);
        icon.setImageDrawable(ContextCompat.getDrawable(context,img));

        Toast toast = new Toast(context);
        toast.setView(v);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();


    }
}
