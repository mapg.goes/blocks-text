package ListView.layout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.blockstext.R;

import java.util.ArrayList;

public class NomeArqAdapter extends ArrayAdapter<NomeArq> {

    private final Context context;
    private final ArrayList<NomeArq> elementos;

    public NomeArqAdapter (Context context, ArrayList<NomeArq> elementos){
        super(context, R.layout.styles_lista, elementos);

        this.context = context;
        this.elementos = elementos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.styles_lista, parent, false);

        TextView nome = view.findViewById(R.id.nomearq);
        nome.setText(elementos.get(position).getNome());

        return view;
    }
}
