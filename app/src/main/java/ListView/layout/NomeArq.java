package ListView.layout;

public class NomeArq {
    private String nome;

    public NomeArq (String nome){

        this.nome =  nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
}
